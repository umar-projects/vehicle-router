﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;
using VehicleRouter.Persistence;

namespace VehicleRouter.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        private readonly VehicleRouterDbContext _db;

        public CustomersController(VehicleRouterDbContext db)
        {
            _db = db;
        }

        public IActionResult Customers()
        {
            return Ok(_db.Customers.ToList());
        }
    }
}