﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VehicleRouter.Persistence;

namespace VehicleRouter.Api.Extensions
{
    public static class ServiceExtensions
    {
        public static IServiceCollection AddNpgsqlContext(this IServiceCollection services, string connectionString)
        {
            services.AddDbContext<VehicleRouterDbContext>(options => options.UseNpgsql(connectionString));
            return services;
        }
    }
}