﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Linq;
using VehicleRouter.Persistence;

namespace VehicleRouter.Api.Data
{
    public class DbCommands
    {
        private readonly string[] args;
        private readonly IHost host;

        public DbCommands(string[] args, IHost host)
        {
            this.args = args;
            this.host = host;
        }

        public void Process()
        {
            using (var scope = host.Services.CreateScope())
            {
                var db = scope.ServiceProvider.GetRequiredService<VehicleRouterDbContext>();
                var mapper = scope.ServiceProvider.GetRequiredService<IMapper>();

                if (args.Contains("dropdb"))
                {
                    Console.WriteLine("Dropping database");
                    db.Database.EnsureDeleted();
                }
                if (args.Contains("migratedb"))
                {
                    Console.WriteLine("Migrating database");
                    db.Database.Migrate();
                }
                if (args.Contains("seeddb"))
                {
                    Console.WriteLine("Seeding database");
                    db.Seed(mapper);
                }
            }

            if (args.Contains("stop"))
            {
                Console.WriteLine("Exiting on stop command");
                Environment.Exit(0);
            }
        }
    }
}