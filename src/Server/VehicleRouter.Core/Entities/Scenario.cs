﻿using System.Collections.Generic;

namespace VehicleRouter.Core.Entities
{
    public class Scenario
    {
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public List<Customer> Customers { get; } = new List<Customer>();
        public long[] Distances { get; set; } = { };
    }
}
