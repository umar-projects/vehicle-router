﻿using System.Collections.Generic;
using System.Linq;
using VehicleRouter.Infrastructure.Vrp.Models;

namespace VehicleRouter.Infrastructure.Vrp.Parsers.Section
{
    public class VrpNodeCoordParser : VrpSectionParser
    {
        public VrpNodeCoordParser(int lineNumber, string[] rawLines, string section) : base(lineNumber, rawLines, section)
        {
        }

        protected override void UpdateVrp(VrpData vrpData, IEnumerable<string> sectionLines)
        {
            vrpData.Nodes = sectionLines.Select(ParseNodeLine).ToList();
        }

        private static Node ParseNodeLine(string nodeLine)
        {
            // parse e.g. 782 51.0678307 3.7290914 GENT
            var parts = nodeLine.Split(" ");
            return new Node
            {
                Id = int.Parse(parts[0]),
                Latitude = double.Parse(parts[1]),
                Longitude = double.Parse(parts[2]),
                Name = parts[3]
            };
        }
    }
}
