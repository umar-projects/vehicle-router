﻿using VehicleRouter.Infrastructure.Vrp.Models;

namespace VehicleRouter.Infrastructure.Vrp.Parsers
{
    public interface IVrpParser
    {
        void UpdateVrp(VrpData vrpData);
    }
}
