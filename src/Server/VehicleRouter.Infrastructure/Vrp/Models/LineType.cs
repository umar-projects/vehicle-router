﻿namespace VehicleRouter.Infrastructure.Vrp.Models
{
    public enum LineType
    {
        Kvp,
        Section,
        Unknown
    }
}
