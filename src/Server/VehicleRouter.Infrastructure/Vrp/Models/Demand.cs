﻿namespace VehicleRouter.Infrastructure.Vrp.Models
{
    public class Demand
    {
        public int NodeId { get; set; }
        public double DemandValue { get; set; }
        public double OpenTimeSeconds { get; set; }
        public double CloseTimeSeconds { get; set; }
        public double ServiceDurationSeconds { get; set; }
    }
}
