﻿using System.Collections.Generic;
using System.Linq;

namespace VehicleRouter.Infrastructure.Vrp.Models
{
    public class VrpData
    {
        public string Name { get; set; } = string.Empty;
        public double Capacity { get; set; }
        public List<int> DepotIds { get; set; } = new List<int>();
        public List<Demand> Demands { get; set; } = new List<Demand>();
        public List<Node> Nodes { get; set; } = new List<Node>();
        public int Dimension { get; set; }
        public long[] DistancesMeters { get; set; } = { };

        public List<(Node, Demand)> GetNodeDemands()
        {
            var nodes = Nodes.OrderBy(n => n.Id);
            var demands = Demands.OrderBy(d => d.NodeId).ToList();

            return nodes.Select((node, i) => (node, demands[i])).ToList();
        }
    }
}
